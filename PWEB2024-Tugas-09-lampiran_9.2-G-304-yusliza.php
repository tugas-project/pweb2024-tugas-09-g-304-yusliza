<!DOCTYPE html>
<html>
<head>
    <title>tugas 9.2</title>
    <style>
        /* Mengatur gaya untuk seluruh halaman */
        body {
            font-family: Arial, sans-serif; /* Mengatur font yang digunakan */
            background-color: #f0f0f0; /* Mengatur warna latar belakang halaman */
            margin: 0; /* Menghilangkan margin default */
            padding: 20px; /* Menambahkan padding di sekitar konten */
        }
        
        /* Mengatur gaya untuk judul halaman */
        h1 {
            text-align: center; /* Menjadikan teks judul berada di tengah */
            color: #333; /* Mengatur warna teks judul */
        }
        
        /* Mengatur gaya untuk setiap hasil operasi */
        .result {
            background-color: #fff; /* Mengatur warna latar belakang elemen hasil */
            border-radius: 5px; /* Menambahkan efek melengkung pada sudut elemen */
            padding: 10px; /* Menambahkan padding di dalam elemen hasil */
            margin-bottom: 10px; /* Menambahkan margin bawah untuk spasi antar elemen hasil */
        }
        
        /* Mengatur gaya untuk teks jawaban "Ya" */
        .yes {
            color: green; /* Mengatur warna teks menjadi hijau */
            font-weight: bold; /* Mengatur teks menjadi tebal */
        }
        
        /* Mengatur gaya untuk teks jawaban "Tidak" */
        .no {
            color: red; /* Mengatur warna teks menjadi merah */
            font-weight: bold; /* Mengatur teks menjadi tebal */
        }
    </style>
</head>
<body>
    <h1>Hasil Operasi Perbandingan dan Aritmatika</h1>
    <?php
    // Mendefinisikan dua variabel dengan nilai masing-masing
    $a = 5;
    $b = 4;
   
    // Mengecek apakah $a sama dengan $b dan menampilkan hasilnya
    echo "<div class='result'>Apakah $a sama dengan $b? Jawaban: <span class='" . ($a == $b ? "yes" : "no") . "'>" . ($a == $b ? "Ya" : "Tidak") . "</span></div>";
    
    // Mengecek apakah $a tidak sama dengan $b dan menampilkan hasilnya
    echo "<div class='result'>Apakah $a tidak sama dengan $b? Jawaban: <span class='" . ($a != $b ? "yes" : "no") . "'>" . ($a != $b ? "Ya" : "Tidak") . "</span></div>";
    
    // Mengecek apakah $a lebih besar dari $b dan menampilkan hasilnya
    echo "<div class='result'>Apakah $a lebih besar dari $b? Jawaban: <span class='" . ($a > $b ? "yes" : "no") . "'>" . ($a > $b ? "Ya" : "Tidak") . "</span></div>";
    
    // Mengecek apakah $a lebih kecil dari $b dan menampilkan hasilnya
    echo "<div class='result'>Apakah $a lebih kecil dari $b? Jawaban: <span class='" . ($a < $b ? "yes" : "no") . "'>" . ($a < $b ? "Ya" : "Tidak") . "</span></div>";
    
    // Mengecek apakah $a sama dengan $b dan lebih besar dari $b dan menampilkan hasilnya
    echo "<div class='result'>Apakah $a sama dengan $b dan lebih besar dari $b? Jawaban: <span class='" . (($a == $b) && ($a > $b) ? "yes" : "no") . "'>" . (($a == $b) && ($a > $b) ? "Ya" : "Tidak") . "</span></div>";
    
    // Mengecek apakah $a sama dengan $b atau lebih besar dari $b dan menampilkan hasilnya
    echo "<div class='result'>Apakah $a sama dengan $b atau lebih besar dari $b? Jawaban: <span class='" . (($a == $b) || ($a > $b) ? "yes" : "no") . "'>" . (($a == $b) || ($a > $b) ? "Ya" : "Tidak") . "</span></div>";
   
    // Mengecek apakah $a lebih besar atau sama dengan $b dan menampilkan hasilnya
    echo "<div class='result'>Apakah $a lebih besar atau sama dengan $b? Jawaban: <span class='" . ($a >= $b ? "yes" : "no") . "'>" . ($a >= $b ? "Ya" : "Tidak") . "</span></div>";
    
    // Mengecek apakah $a lebih kecil atau sama dengan $b dan menampilkan hasilnya
    echo "<div class='result'>Apakah $a lebih kecil atau sama dengan $b? Jawaban: <span class='" . ($a <= $b ? "yes" : "no") . "'>" . ($a <= $b ? "Ya" : "Tidak") . "</span></div>";
    
    // Menampilkan hasil penjumlahan $a dan $b
    echo "<div class='result'>Hasil penjumlahan $a dan $b: " . ($a + $b) . "</div>";
    
    // Menampilkan hasil pengurangan $a dan $b
    echo "<div class='result'>Hasil pengurangan $a dan $b: " . ($a - $b) . "</div>";
    
    // Menampilkan hasil perkalian $a dan $b
    echo "<div class='result'>Hasil perkalian $a dan $b: " . ($a * $b) . "</div>";
    
    // Menampilkan hasil pembagian $a dengan $b
    echo "<div class='result'>Hasil pembagian $a dengan $b: " . ($a / $b) . "</div>";
    
    // Menampilkan sisa pembagian $a dengan $b
    echo "<div class='result'>Sisa pembagian $a dengan $b: " . ($a % $b) . "</div>";
    ?>
</body>
</html>
