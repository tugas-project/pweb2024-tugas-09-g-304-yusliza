<!DOCTYPE html>
<html>
<head>
    <title>Informasi Gaji Karyawan 9.1</title>
    <style>
        body {
            font-family: Arial, saphns-serif;
            background-color: #f0f0f0;
            margin: 0;
            padding: 20px;
        }
        h1 {
            text-align: center;
            color: #333;
        }
        .container {
            max-width: 400px;
            margin: 0 auto;
            background-color: #fff;
            border-radius: 8px;
            padding: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        p {
            margin: 10px 0;
            font-size: 16px;
        }
        .highlight {
            color: #007bff; /* Warna teks biru */
            font-weight: bold; /* Teks tebal */
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Informasi Gaji Karyawan</h1>
        <?php
        // Mendefinisikan nama karyawan secara statis
        $nama_karyawan = "Andi";

        // Mendefinisikan variabel gaji
        $gaji = 1000000;

        // Mendefinisikan persentase pajak
        $pajak = 0.1;

        // Menghitung jumlah pajak yang harus dibayar
        $jumlah_pajak = $gaji * $pajak;

        // Menghitung gaji yang dibawa pulang setelah dikurangi pajak
        $gaji_bersih = $gaji - $jumlah_pajak;
        ?>

        <!-- Menampilkan informasi karyawan dan gaji -->
        <p>Nama Karyawan: <span class="highlight"><?php echo $nama_karyawan; ?></span></p>
        <p>Gaji Sebelum Pajak: <span class="highlight">Rp. <?php echo $gaji; ?></span></p>
        <p>Jumlah Pajak: <span class="highlight">Rp. <?php echo $jumlah_pajak; ?></span></p>
        <p>Gaji yang Dibawa Pulang: <span class="highlight">Rp. <?php echo $gaji_bersih; ?></span></p>
    </div>
</body>
</html>


